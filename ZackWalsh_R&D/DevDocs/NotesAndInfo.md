# Basic Information and Notes For This Folder

## VS Code Helpers and Useful Extensions
- Robot Framework Intellisense:
  - VS Marketplace Link: [Robot Framework Intellisense](https://marketplace.visualstudio.com/items?itemName=TomiTurtiainen.rf-intellisense)
- Robocorp Code
  - VS Marketplace Link: [Robocorp Code](https://marketplace.visualstudio.com/items?itemName=robocorp.robocorp-code)

<br/>

---

<br/>

## Main Python Dependencies are as follows: 
- Python 3.9.5 or newer
- RCC - The Robocorp Robot Launcher. Offers a Cloud service but I'm not a huge fan of it. Using it locally is much more time efficent and uses fewer system resources.
  - No matter how you install RCC, it MUST be put into your path somehow. I usually just drop it into the python path. You can find it by opening a CMD, and issung this command
    ``` 
    >>> import os
    >>> os.path

    The PATH of the python install location is returned over here.
    ```
  - Robot Launcher. Not totally needed but very useful Most projects I build rely on this. 
  - Download it from here: [RCC Latest Version](https://downloads.robocorp.com/rcc/releases/latest/windows64/rcc.exe)
  - Or if you're a ~poweruser~ use this curl command:
       
         curl -o rcc.exe https://downloads.robocorp.com/rcc/releases/latest/windows64/rcc.exe

- Here's a list of all the python packages used.  Each package is not really version locked. But if issues come up, the versions I've used will be thrown into this chart. To Install all the packages below in one shot use this command:
          
      pip install robot rpaframework tensorflow keras numpy pillow scipy h5py matplotlib opencv-python keras-resnet imageai labelimg varname

<center>

| Package Name   | Use Of Package  | Version Number
|---|---|---|
| RobotFramework  |  Robot task execution  | 4.0.0 |
| RPAFramework | Desktop RPA actions | 9.2.* |
| Tensorflow | ML Lib and helpers | N/A |
| Keras |  Image Recgonition and ML Helpers | N/A |
| Numpy | Math Lib  | N/A |
| Pillow | More Math | N/A |
| SciPy | SCIENCE BITCH | N/A |
| h5py | No idea. Helper/Dependency? | N/A |
| Matplotlib | Plotting lib used mostly with numpy helpers | N/A |
| Opencv-Python | Open CV injection for python. Image drawing/generation  | N/A |
| Keras-Resnet | Keras extensions. | N/A |
| ImageAI |Object Recgonition Library used with Tensorflow | N/A |
| LabelImg | Used for setting up tensofrlow datasets | N/A |
| varname | Used to pull names of variables. Like reflection for python |

</center>

<br/>

---

<br/>

## Setting Up Tensorflow Object Dectection APIs
- Protobuf Setup and Compile Directions
  - Download/Clone the tensorflow models reop and then put it somewhere useful. This is going to be a commonly used directory. 
  - This folder is inside my R&D Folder but you can fresh clone it from the models repo in Git if wanted. 
  - Download and Copy the protobuf folder from inside tensorflow into your path. This is used by tesnorflow for image detection.  If you need a newer version just google protobuf and find the latesr python zip inside their git repo. Or if you're really that lazy, just [click here](https://github.com/protocolbuffers/protobuf/releases/download/v3.17.0/protoc-3.17.0-win64.zip)
  - You can take the whole folder and add it to the path environment. Just make sure you include the path to the EXE itself in the path variable. Not just down to the base dir of the folder.  
    - Example: ```C:\Users\zachary\source\repos\autonomous-development\ZackWalsh_R&D\Tensorflow\protoc\bin``` is a good path value.
    - But doing ```C:\Users\zachary\source\repos\autonomous-development\ZackWalsh_R&D\Tensorflow\protoc\``` is **NOT** a good path value.
  - From there, CD into the models/research directory inside the tensorflow directory and issue the following command. If this doesn't work, make sure you restart your shell session to import the protobuf install to your path variables. 
  
    ```
    for /f %i in ('dir /b object_detection\protos\*.proto') do protoc object_detection\protos\%i --python_out=.
    ```
- COCO API Install/Setup
  - First up is to install the cyton tools for the current platform. These require the VCC 2015 command line tools. 
  - If you do not have them, install them by clicking [this link here](https://go.microsoft.com/fwlink/?LinkId=691126)
  - You can get them by running the following commands
    
    ``` 
    pip install cython
    pip install git+https://github.com/philferriere/cocoapi.git#subdirectory=PythonAPI
    ```
- Setup Object Detection API for Tensorflow
  - Once the previous steps are done, you can install the Object Detection API using the following commands. These commands are run from inside Tensorflow/Models/Research like the other commands used. 
  - If the CP command fails, it's because you're on windows. Just copy the setup.py from the folder mentioned above to the models/research directory.  Then run the pip command. 
  - Or I think you can just CD Down to that folder and everything should build correctly. But either way, you just need to run the setup file via pip so we pull in all the dependencies.

    ```
    cp object_detection/packages/tf2/setup.py .
    python -m pip install .
    ```
  - If/when this fails, check the install instructions for pycocotools. This will help fix this issue while installing the API. These directions can be found [here.](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html#tf-models-install-coco)
  - Once you have all of that done, you can test this installation by running this command from inside the same models/resarch directory. 
    ```
    python object_detection/builders/model_builder_tf2_test.py
    ```

<br/>

---

<br/>