# This class pulls on the pillow helper libs I've written and allows us to make a couple test calls.
import sys                                   # Used for arg pulling
import cv2                                   # Used to print out images to a window.
import os                                    # File path opertions
import shutil                                # Remove dirs and subfolders
from ImgContentOperations import *           # Removes a BG color from an image.
from RPA.Desktop.Windows import Windows      # Used to open apps and get Screencaps.

# Begin here by getting a new TIS Instance spun up and pull in a screencap of it.
# Make new window controller and make a whole bunch of operations to use with it
WinController = Windows() 

# Kill our old TIS instance if it exists.
process_status = WinController.process_exists("mainmenu", False)
if process_status:
    print ("KILLING OLD TIS INSTANCE...")
    WinController.kill_process(process_status.name())
    print ("DONE OK!\n")

# Remove old dirs from the output folder.
print ("REMOVING OLD DIRS AND OUTPUT DATA...")
try: shutil.rmtree("images")
except: None
try: shutil.rmtree("RuntimeData")
except: None

try: os.mkdir("RuntimeData")
except: None
print ("DONE OK!\n")

# Open a new TIS Instance and then get our TIS Window instance. Waiting 200 Seconds for now.
print ("OPENING TECHSTREAM...")
print ("--> LAUNCHING TIS EXECUTABLE AND GETTING WINDOW LOCATION")
techstream_id = WinController.open_from_search("Techstream", "Techstream (Ver", 200, True)  
print ("OPENED OK!\n")

# Now we wanna take the Techstream window, and get the controls from it while taking 
# Screen grabs from it all at once. 
# Store a path for this TIS Screen cap since we're going to use that path many times.
print ("TAKING SCREENSHOT OF TECHSTREAM AND GETTING CONTROLS NOW...")
WinController.screenshot("TechstreamScreenCap.png")
shutil.copy("images/TechstreamScreenCap.png", os.getcwd() + "\\RuntimeData\\TechstreamScreenCap.png")
tis_screenshot_path = os.getcwd() + "\\RuntimeData\\TechstreamScreenCap.png"
print ("--> REMOVING IMAGES TEMP DIR")
try: shutil.rmtree("images")
except: None
print ("--> SCREENSHOT DONE OK!")
techstream_window_elems = WinController.get_window_elements(False, False, True)
print ("--> CONTROLS PULLED OUT OK!")
print ("DONE OK!\n")

# Close out the TIS Window.
print ("CLOSING WINDOWS NOW...")
print ("--> KILLING TIS APP WITH WINDOW CONTROLLER")
WinController.close_all_applications()
print ("DONE OK!\n")

# Extract contours here and show them to the user.
print ("EXTRACTING SHAPES FROM MAIN TIS WINDOW NOW...")
ShapePuller = ImgContentOperations(tis_screenshot_path)
print (f"--> STORING SCREENSHOT AS: {tis_screenshot_path}")
print ("--> STORING CONTENTS INTO AN ARRAY FILE NOW...")
print ("--> FINDING SHAPE CONTOURS NOW...")
ShapePuller.extract_shape_contours(tis_screenshot_path)
print (f"    --> TOTAL OF: {len(ShapePuller.FOUND_SHAPE_CONTOURS)} CONTROL SHAPES FOUND IN THE IMAGE")
print (f"    --> SAVING CONTENT INTO: {ShapePuller.CONTOUR_OUTPUT_IMG}")
print ("--> SHOWING SHAPES NOW...")
print ("--> BASE PIXEL DATA IS SAVING IN THE BACKGROUND. THIS MAY TAKE A BIT")
cv2.imshow('Contour Shape Extraction', ShapePuller.CONTOUR_CONTENT_CV2)  
ShapePuller.array_to_file(ShapePuller.BASE_IMAGE, "BASE_IMAGE_CONTENT.txt")            
cv2.waitKey(0)
print ("DONE OK!\n")