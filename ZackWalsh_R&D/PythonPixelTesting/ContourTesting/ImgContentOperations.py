import os                   # File operations and helpful sys actions.
import glob                 # File Searching and indexing.
import numpy                # Math and array operartions for image data processing.
from varname import nameof  # Used to get names of variables.
from PIL import Image       # Used for pixel minipulation.
import asyncio              # Async/parallel operations
import cv2                  # OpenCV for image detection

class ImgContentOperations:
    '''
        This class is setup to convert an input image item into an array of pixel values. 
        We then use this data inside an algo for recursion to find us an image inside an image
        by looking for where the contents of one image are inside the other one.
    '''

    # Class variables
    BASE_IMAGE = None                 # Path for the base input value.
    BASE_IMAGE_CONTENT_NUMPY = None   # 2d Numpy array for input image contents.
    BASE_IMAGE_CONTENT_CV2 = None     # 2d Numpy array for input image contents.

    CONTOUR_OUTPUT_IMG = None         # File output from greyscale data.
    CONTOUR_CONTENT_NUMPY = None      # Content/data of the window when processed (NUMPY)
    CONTOUR_CONTENT_CV2 = None        # Content/data of the window when processed with (CV2)

    MAX_TOLERANCE = 128               # Number from 0-255 for the maximum possible difference found for pixels.
    FOUND_SHAPE_CONTOURS = []         # Contour shapes pulled using OpenCV


    # Constructor for all the stuff needed to import and search our images.
    # Pass in only one image path here.
    def __init__(self, base_image_path):

        # When you go to run this in a py script, it may help to print this stuff out to the console.
        self.BASE_IMAGE = base_image_path    
        self.BASE_IMAGE_CONTENT_NUMPY = self.load_image_contents(self.BASE_IMAGE)  
        # self.array_to_file(self.BASE_IMAGE, "BASE_IMAGE_CONTENT.txt")            

    # Takes in a numpy array and saves it to an output file. 
    # Pass in an output file name if desired. Otherwise a new dir is made for saving. 
    def array_to_file(self, path_to_image, output_file_name):
        # Store image content into array here.
        numpy_array_set = self.load_image_contents(path_to_image)

        # Check for path split and make sure we do not have an extension.
        hasExtension = "." in output_file_name
        if (hasExtension is not True): output_file_name = output_file_name + ".txt"

        # As long as we have these conditions we can run this block
        isFullPath = "/" in output_file_name or "\\" in output_file_name
        if (isFullPath is not True):
            dir_head, file_name = os.path.split(output_file_name)
            file_name_only = os.path.splitext(file_name)[0]
            output_file_name = os.getcwd() + "\\RuntimeData\\" + file_name_only + ".txt"

        # Reshape this array to a 2d size.
        # To re read this and resize it back into a 2d shape, 
        #   loaded_arr = gfg.loadtxt("geekfile.txt") --> file name .txt here
        #   load_original_arr = loaded_arr.reshape(
        #       loaded_arr.shape[0], loaded_arr.shape[1] // arr.shape[2], arr.shape[2])
        data_reshaped = numpy_array_set.reshape(numpy_array_set.shape[0], -1)

        # Save our array here.
        # This writes to the output file name we setup before. 
        numpy.savetxt(output_file_name, data_reshaped, delimiter=" | ") 


    # Pass in an image path and convert it into a 2D Array.
    # This returns a numpy array which allows us to compare contents of images. 
    def load_image_contents(self, path_to_image = ""):
        # Store the contents here.
        if (path_to_image == ""): path_to_image = self.BASE_IMAGE
        image_contents = Image.open(path_to_image)
        numpy_contents = numpy.array(image_contents)

        # If working on Self. Update values.
        if (self.BASE_IMAGE == path_to_image):
            self.BASE_IMAGE_CONTENT_NUMPY = numpy_contents

        # Return those contents here.
        return numpy_contents


    # Takes an input image and converts it into a wireframe output value. 
    # Pass in the path to the image we want to convert over here.
    # If there was a class item made with a valid path, this is stored already.
    # This will return to us an X/Y pair for the middle of each shape found.
    # @ __background
    def extract_shape_contours(self, image_path = ""):
        # Store the image contents.
        image_data_old = cv2.imread(image_path)
        if (image_path == self.BASE_IMAGE): self.BASE_IMAGE_CONTENT_CV2 = image_data_old

        # Get a greyscale conversion here.
        image_grayscale = cv2.cvtColor(image_data_old, cv2.COLOR_BGR2GRAY)
        _, threshold = cv2.threshold(image_grayscale, 127, 255, cv2.THRESH_BINARY)
        
        # Generate objects with a findContours function and get them here
        located_contours, _ = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        image_data_new = image_data_old
        if (image_path == self.BASE_IMAGE): self.FOUND_SHAPE_CONTOURS = located_contours

        # Loop the contours and draw them out to a new image.
        # Do basic operations here. 
        #   1) Approximate shape contour size.
        #   2) Draw the contour
        #   3) Get the middle of a control/shape.
        #   4) Show window/contour
        for countour_index in range(1, len(located_contours)):
            # Store the current shape we're processing and find the approximate shape for it.
            current_shape_item = located_contours[countour_index]
            approx_shape = cv2.approxPolyDP(current_shape_item, 0.03 * cv2.arcLength(current_shape_item, True), True)

            # Draw the contour shape here and find the middle of the shape drawn.
            shape_middle = cv2.moments(current_shape_item)
            if shape_middle['m00'] != 0.0:
                xMiddle = int(shape_middle['m10']/shape_middle['m00'])
                yMiddle = int(shape_middle['m01']/shape_middle['m00'])

            # Draw the text of the number of the control found here.
            # Place that in the middle of the control.
            # Rectangles get drawn in green and all other shapes are drawn in grey.
            if len(approx_shape) == 4:
                cv2.drawContours(image_data_new, [current_shape_item], 0, (0, 255, 0), 5)
                cv2.putText(image_data_new, 'Rectangle Located (Control #'  + str(countour_index) + ')', (xMiddle, yMiddle),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 255), 2)

            # else:
            #     cv2.drawContours(image_data_new, [current_shape_item], 0, (190, 190, 190), 3)
            #     cv2.putText(image_data_new, 'Control #' + str(countour_index), (xMiddle, yMiddle),
            #         cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 255), 2)
            
        
        # Save this output as a new image item here.
        dir_head, file_name = os.path.split(image_path)
        image_name_only = os.path.splitext(file_name)
        new_img_path = os.getcwd() + "\\RuntimeData\\" + image_name_only[0] + "_CONTOUR_SHAPES" + image_name_only[1]
        cv2.imwrite(new_img_path, image_data_new)

        # Save class values if needed.
        if (image_path == self.BASE_IMAGE):
            self.CONTOUR_OUTPUT_IMG = new_img_path
            self.CONTOUR_CONTENT_CV2 = image_data_new
            self.CONTOUR_CONTENT_NUMPY = self.load_image_contents(new_img_path)

        # Give back the path of our new image.
        return new_img_path, image_data_new, image_data_old
        

    # Takes in a numpy array of the source image content and then an array of the content we want to locate here.
    # We do a baseline comparision check to see how close the two objects are related to one another.
    # Then try to tune the color of the content to find to locate that in our images. 
    # This returns a few things.
    #   1) How close we are to finding the content inside the source
    #   2) If we found something, what are the cords of that object (4 points)
    #   3) If we found something, what is the center/closest middle point of that object.
    def find_contents_inside_source(source_contents, content_to_find):
        return
