import os                   # File operations and helpful sys actions.
import numpy                # Math and array operartions for image data processing.
import cv2                  # OpenCV for image detection
import shutil               # Used for recursive dir removal.
from PIL import Image       # Used for pixel minipulation.
from SubImageWalker import *  # Pixel walker

class ImgArrayMutation:

    # Class variables
    BASE_IMAGE_PATH = None      # Path for the base input value.
    BASE_ARRAY_TWO_D = None     # 2d Numpy array for input image contents.
    BASE_ARRAY_THREE_D = None   # 3D Numpy array for input image contents.

    CHUNK_ROW_COUNT = 10        # Default Chunk Row Count
    CHUNK_COLL_COUNT = 10       # Default Chunk Coll Count.

    # Loads up an image file and stores the content of it in a text file.
    # Stores the content of the file in a numpy array and keeps the path of the file.
    def __init__(self, path_to_file, chunk_size = None):
        # When you go to run this in a py script, it may help to print this stuff out to the console.
        self.BASE_IMAGE_PATH = path_to_file    
    
        # Store contents of the image bits here.
        array_sets = self.load_image_contents()
        shutil.copy(self.BASE_IMAGE_PATH, os.getcwd() + "\\RuntimeData\\BASE_INPUT_IMAGE.png")

        self.BASE_ARRAY_TWO_D = array_sets[0]
        self.BASE_ARRAY_THREE_D = array_sets[1]

        # Store chunk size set.
        if (chunk_size is not None):
            self.CHUNK_ROW_COUNT = chunk_size[0]
            self.CHUNK_COLL_COUNT = chunk_size[1]

    # Takes a 3d matrix and compresses the value for each iteration of that matrix into one level.
    # Used for going from R, G, B channles into a single RGB Matrix file.
    def __get_two_d_tuple_set(self, numpy_three_d):
        npy_two_d = []
        two_dim_size = (numpy_three_d.shape[0], numpy_three_d.shape[1])
        npy_two_d = numpy.zeros(two_dim_size, dtype=object)

        next_value_set = ""

        for row_count in range(0, numpy_three_d.shape[0]):
            for col_count in range(0, numpy_three_d.shape[1]):
                for set_count in range(0, numpy_three_d.shape[2]):
                    # Store a temp item.
                    array_set_item = numpy_three_d[:,:,set_count]
                    next_value_set = next_value_set + str(array_set_item[row_count][col_count])

                    # Check for last loop.
                    if set_count != numpy_three_d.shape[2] - 1: next_value_set += ", " 

                # Savw the value here and reset the next value.
                npy_two_d[row_count][col_count] = next_value_set
                next_value_set = ""

        # Throw back the matrix here.
        return npy_two_d


    # Pass in an image path and convert it into a 3D Array.
    # This returns a numpy array which allows us to compare contents of images. 
    def load_image_contents(self):
        # Store the contents here.
        image_contents = cv2.imread(self.BASE_IMAGE_PATH)
        # image_contents.convert("RGB")

        # Make 3D array here.
        npy_three_d = numpy.array(image_contents, ndmin=3)

        # Setup the 2d Array here.
        npy_two_d = self.__get_two_d_tuple_set(npy_three_d)

        # Return those contents here.
        return npy_two_d, npy_three_d

    # Takes in a numpy array and saves it to an output file. 
    # Pass in an output file name if desired. Otherwise a new dir is made for saving. 
    def array_to_file(self, array_to_print, output_file_name):
        # Check for path split and make sure we do not have an extension.
        hasExtension = "." in output_file_name
        if (hasExtension is not True): output_file_name = output_file_name + ".txt"

        # As long as we have these conditions we can run this block
        isFullPath = ":\\" in output_file_name or ":/" in output_file_name
        if (isFullPath is not True):
            dir_head, file_name = os.path.split(output_file_name)
            if (len(dir_head)) != 0: 
                output_file_name = os.getcwd() + "\\RuntimeData\\" + dir_head + "\\" + file_name
            else: 
                output_file_name = os.getcwd() + "\\RuntimeData\\" + file_name
            if ("." not in output_file_name): output_file_name = output_file_name + ".txt"

        # IF we do not have a 2d Array yet, then make one now from the input array size.
        if (array_to_print.ndim == 3):
            array_to_print = self.__get_two_d_tuple_set(array_to_print)

        # Save our array here.
        # This writes to the output file name we setup before.
        # Temp no formatting: fmt="%s"
        numpy.savetxt(output_file_name, array_to_print, delimiter = " | ", fmt="%s")

        # Gives back the main output file name from the generated path here.
        return output_file_name


    # Takes an array input and saves the output file as a png file based on the input data.
    # Returns nothing.
    def save_image_tiles(self, output_file_name, subimage_height = None, subimage_width = None, show_live_update = False):
        # Check the size values here.
        if subimage_height == None: subimage_height = self.CHUNK_ROW_COUNT
        if subimage_width == None: subimage_width = self.CHUNK_COLL_COUNT

        # As long as we have these conditions we can run this block
        isFullPath = ":\\" in output_file_name or ":/" in output_file_name
        if (isFullPath is not True):
            dir_head, file_name = os.path.split(output_file_name)
            if (len(dir_head) != 0): 
                output_file_name = os.getcwd() + "\\RuntimeData\\ChunkImageData\\" + dir_head + "\\" + file_name
            else: 
                output_file_name = os.getcwd() + "\\RuntimeData\\ChunkImageData\\" + file_name

        # Array for all image data, the base image to split, and the output name.
        all_image_data = []
        base_output_file = output_file_name

        # Reset input data
        array_sets = self.load_image_contents()
        image_data = array_sets[1]

        # Write the base output file.
        segmented_output_file = os.getcwd() + "\\RuntimeData\\" + "BASE_CV2_SUBREGIONS.png"

        # Save the image shape here
        imgheight=image_data.shape[0]
        imgwidth=image_data.shape[1]

        # Get the y and x starting points
        y1 = 0
        M = imgheight // subimage_height
        N = imgwidth // subimage_width

        # Preview window size.
        half_width = int(imgwidth / 2)
        half_height = int(imgheight / 2)

        # Loop the values of the pixels here.
        for y in range(0,imgheight,M):
            for x in range(0, imgwidth, N):
                # Extract data here
                y1 = y + M      # Top left
                x1 = x + N      # Left edge
                start_point = (x, y)
                end_point = (x1, y1)
                seg_pixel_set = image_data[y:y+M,x:x+N]
                
                # Write segment base output.
                output_file_name = base_output_file + "_" + str(x1) + "_" + str(y1) + ".png"
                cv2.imwrite(output_file_name, seg_pixel_set)        # Segment Pixels no points.
                all_image_data.append(seg_pixel_set)

                # Write region output and draw a pixel bounding box.
                subregion_border_color = (10,10,225)
                image_data = cv2.rectangle(image_data, start_point, end_point, subregion_border_color, 5)      
                cv2.imwrite(segmented_output_file, image_data)      # Region output only.

                # Get the walker output and print out here.
                PointWalker = SubImageWalker(int(self.CHUNK_ROW_COUNT * 2))
                _, seg_pixel_set_walked = PointWalker.generate_start_points(seg_pixel_set)
                walker_seg_file_name = PointWalker.WALKER_OUTPUT_FILE + "WALKER_OUTPUT_" + str(x1) + "_" + str(y1) + ".png"
                cv2.imwrite(walker_seg_file_name, seg_pixel_set_walked)     # Walker region output only (will include border)

                # Update image preview.
                if (show_live_update):
                    image_data_small = cv2.resize(image_data, (half_width, half_height))                   
                    cv2.imshow("Output Segments", image_data_small)    
                    cv2.waitKey(1)


        # Return the set of all image points
        image_data_small = cv2.resize(image_data, (half_width, half_height))                   
        cv2.imshow("Output Segments", image_data_small)   
        cv2.waitKey(1)
        return image_data, all_image_data
