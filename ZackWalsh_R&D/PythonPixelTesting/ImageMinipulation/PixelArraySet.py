from os import getcwd
import numpy                # Math and array operartions for image data processing.

class PixelArraySet:
    """ 
        Does simple move operations to an array of pixel data based on the move type and 
        the dataset provided
    """

    # Class Variables
    START_LOCATION = []
    BASE_PIXEL_SET = []
    GENERATED_PIXEL_SET = []

    # Makes an array operation value set of a 3x3 numpy array with the values around it.
    def __init__(self, segment_pixels, start_location): 
        # Store the segment pixels and base location
        self.BASE_PIXEL_SET = segment_pixels
        self.START_LOCATION = start_location

        # Store values here.
        generated_array = self.populate_array_values()
        self.GENERATED_PIXEL_SET = generated_array

    # Finds the 8 points around a main array object and populates them.
    # Then sets up a bunch of different values for the pixel array.
    def populate_array_values(self):
        # Make a 3x3 array of zeros. 9 points total. Then set the middle and store it.
        pixel_value_array = numpy.zeros(3,3)    
        pixel_value_array[1][1] = self.BASE_PIXEL_SET[self.START_LOCATION[0]][self.START_LOCATION[1]]

        # Now find the values in all directions outside this array.
        start_x_value = self.START_LOCATION[0]
        start_y_value = self.START_LOCATION[1]

        # Subtract/add one in all directions.
        # This builds us all of the array values here.

        # Top Left
        try: pixel_value_array[0][0] = self.BASE_PIXEL_SET[start_x_value - 1][start_y_value + 1]
        except: pixel_value_array[0][0] = None

        # Top Middle
        try: pixel_value_array[1][0] = self.BASE_PIXEL_SET[start_x_value][start_y_value + 1]
        except: pixel_value_array[1][0] = None

        # Top Right
        try: pixel_value_array[2][0] = self.BASE_PIXEL_SET[start_x_value + 1][start_y_value + 1]
        except: pixel_value_array[2][0] = None

        # Middle Left
        try: pixel_value_array[0][1] = self.BASE_PIXEL_SET[start_x_value - 1][start_y_value]
        except: pixel_value_array[0][1] = None

        # Middle Right
        try: pixel_value_array[2][1] = self.BASE_PIXEL_SET[start_x_value + 1][start_y_value]
        except: pixel_value_array[2][1] = None

        # Bottom Left
        try: pixel_value_array[0][2] = self.BASE_PIXEL_SET[start_x_value - 1][start_y_value - 1]
        except: pixel_value_array[0][2] = None

        # Bottom Middle
        try: pixel_value_array[1][2] = self.BASE_PIXEL_SET[start_x_value][start_y_value - 1]
        except: pixel_value_array[1][2] = None

        # Bottom Right
        try: pixel_value_array[1][2] = self.BASE_PIXEL_SET[start_x_value + 1][start_y_value - 1]
        except: pixel_value_array[1][2] = None

        # Store these values in self and return them.
        self.GENERATED_PIXEL_SET = pixel_value_array
        return pixel_value_array
    

    # Sets the new start location for this array and fills in the values around the middle point.
    def update_start_location(self, start_location):
        # Make a new set of arrays here and store new start location
        self.START_LOCATION = start_location
        generated_set = self.populate_array_values()

        # Return this new set of values
        self.GENERATED_PIXEL_SET = generated_set
        return generated_set

    # region Update Location Moves
    # Moves n squares to the right and updates pixel values.
    def move_left(self, steps_to_move = 1):
        self.START_LOCATION[0] -= steps_to_move
        self.GENERATED_PIXEL_SET = self.update_start_location(self.START_LOCATION)
    
    # Moves n square to the right and updates the pixel values
    def move_right(self, steps_to_move = 1):
        self.START_LOCATION[0] += steps_to_move
        self.GENERATED_PIXEL_SET = self.update_start_location(self.START_LOCATION)

    # Moves n squares up and updates pixel values
    def move_up(self, steps_to_move = 1):
        self.START_LOCATION[1] += steps_to_move
        self.GENERATED_PIXEL_SET = self.update_start_location(self.START_LOCATION)

    # Moves n squares down and updates pixel values
    def move_down(self, steps_to_move = 1):
        self.START_LOCATION[1] += steps_to_move
        self.GENERATED_PIXEL_SET = self.update_start_location(self.START_LOCATION)
 
    # Moves n diagonal jumps to the top left
    def move_up_left(self, steps_to_move = 1):
        self.START_LOCATION[0] -= steps_to_move
        self.START_LOCATION[1] += steps_to_move
        self.GENERATED_PIXEL_SET = self.update_start_location(self.START_LOCATION)

    # Moves n diagonal jumps to the bottom left
    def move_down_left(self, steps_to_move = 1):
        self.START_LOCATION[0] -= steps_to_move
        self.START_LOCATION[1] -= steps_to_move
        self.GENERATED_PIXEL_SET = self.update_start_location(self.START_LOCATION)

    # Moves n diagonal jumps to the top right
    def move_up_right(self, steps_to_move = 1):
        self.START_LOCATION[0] += steps_to_move
        self.START_LOCATION[1] += steps_to_move
        self.GENERATED_PIXEL_SET = self.update_start_location(self.START_LOCATION)

    # Moves n diagonal jumps to the bottom right
    def move_down_right(self, steps_to_move = 1):
        self.START_LOCATION[0] += steps_to_move
        self.START_LOCATION[1] -= steps_to_move
        self.GENERATED_PIXEL_SET = self.update_start_location(self.START_LOCATION)
    # endregion