import shutil                       # File Copying
import os                           # Path operations
from ImgArrayMutation import *      # Image minip   
from SubImageWalker import *        # Array Generation

# Old output cleanup
try: shutil.rmtree("RuntimeData")
except: None
try: os.mkdir("RuntimeData")
except: None

# Generate arrays and output text file for the main numpy output based on input file.
tis_test = os.path.dirname(os.getcwd()) + "\\StaticData\\TechstreamScreenCap.png"
bitmap_test = os.path.dirname(os.getcwd()) + "\\StaticData\\BasicFormObjectDetection.bmp"
ImageMutation = ImgArrayMutation(tis_test, (5,5))     
output_numpy_file = ImageMutation.array_to_file(ImageMutation.BASE_ARRAY_TWO_D, "BASE_OUTPUT_CONTENTS.txt")

# Show this output info here.
print (f"IMAGE PROCESSED AND ARRAYS GENERATED OK!")
print (f"--> 2D ARRAY POINTS:   {ImageMutation.BASE_ARRAY_TWO_D.size}")
print (f"--> 2D ARRAY SIZE:     {ImageMutation.BASE_ARRAY_TWO_D.shape}")
print (f"--> 3D ARRAY POINTS:   {ImageMutation.BASE_ARRAY_THREE_D.size}")
print (f"--> 3D ARRAY SIZE:     {ImageMutation.BASE_ARRAY_THREE_D.shape}")
print (f"--> Output File:       {output_numpy_file}")
print ("")

# Now we want to create arrays from the 2d output one in any split number of chunks
os.mkdir("RuntimeData\\ChunkImageData")
split_image_preview, array_split_set = ImageMutation.save_image_tiles(
    "OUTPUT_CHUNK",
    ImageMutation.CHUNK_ROW_COUNT, ImageMutation.CHUNK_COLL_COUNT, 
    True
)
print ("GENERATED ARRAY CHUNKS OK!")
print (f"--> CHUNK COUNT:       {len(array_split_set)}")
print (f"--> MAIN CHUNK SHAPE:  {array_split_set[0].shape}")
print (f"--> POINTS PER CHUNK:  {array_split_set[0].size}")
print (F"--> NUMBER OF POINTS:  {array_split_set[0].size * len(array_split_set)}")
print ("")

# Store each one of the chunks out to a file now.
print ("GENERATING NEW OUTPUT FILE SETS FOR NUMPY ARRAY CHUNK VALUES. THIS MAY TAKE A BIT")
os.mkdir("RuntimeData\\ChunkTextData")
for list_indexer in range(0, len(array_split_set)):
    # Store a temp name and an array to output.
    array_name = "ChunkTextData\\ARRAY_CHUNK_" + str(list_indexer + 1) + ".txt"
    array_to_print = array_split_set[list_indexer]

    # Run the printer here.
    # two_d_array_to_print = ImageMutation.__get_two_d_tuple_set(array_to_print)
    ImageMutation.array_to_file(array_to_print, array_name)

    # Print status for each item here.
    if (list_indexer % ImageMutation.CHUNK_ROW_COUNT != 0 and list_indexer + 1 != len(array_split_set)): continue
    print (f"--> Array {list_indexer + 1} of {len(array_split_set)} complete (File: {array_name})")
print ("DONE OK!\n")