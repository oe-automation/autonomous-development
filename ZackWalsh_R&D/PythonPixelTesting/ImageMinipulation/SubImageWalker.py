import os                   # File operations and helpful sys actions.
import numpy                # Math and array operartions for image data processing.
import cv2                  # OpenCV for image detection
import random               # Point picking random.
from PIL import Image      # Used for pixel minipulation.

# NOTE: NO ACTUAL IMAGE SAVING SHOULD HAPPEN HERE! WE PASS AROUND THE PIXEL ARRAYS 
#       AND MODIFY THEM. RETURN THEM BACK TO BE WRITTEN LATER
class SubImageWalker:
    
    # Class variables
    WALKER_START_POINTS = 5    # Number of spots to walk from.
    WALKER_START_LOCATION = []  # Locations of the walker start points.

    # Loads up an image file and stores the content of it in a text file.
    # Stores the content of the file in a numpy array and keeps the path of the file.
    def __init__(self, start_point_count = 0):
        # Set output file name.
        self.WALKER_OUTPUT_FILE = os.getcwd() + "\\RuntimeData\\WalkerImageData\\"

        # Set point start count.
        if start_point_count != 0: 
            if start_point_count < 10: start_point_count = 10
            self.WALKER_START_POINTS = start_point_count

    
    # Make the start points of the walkers here.
    # Generates a number of random points for the walker to start at.
    def generate_start_points(self, segment_pixels, walker_starts = 0):
        # Remove old output
        try: os.mkdir(self.WALKER_OUTPUT_FILE)
        except: None

        # Find 10 (Set by walker start points) random points.
        if (walker_starts) != 0: self.WALKER_START_POINTS = walker_starts
        else: walker_starts = self.WALKER_START_POINTS

        # Make a numpy array out of the input image data
        max_x_value = segment_pixels.shape[0]
        max_y_value = segment_pixels.shape[1]

        # Pick 10 points/walker start point count
        start_points = []
        for walker_point in range(0, walker_starts):
            x_start = random.randrange(0, max_x_value, walker_starts)
            y_start = random.randrange(0, max_y_value, walker_starts)

            # Store it as a tuple.
            point_set = (x_start, y_start)
            start_points.append(point_set)

            # Draw the bounding box/start point location.
            rect_dims = 10
            rect_start = (x_start - rect_dims, y_start - rect_dims)
            rect_end = (x_start + rect_dims, y_start + rect_dims)
            fill_color = (20, 150, 10)
            border_color = (200, 0, 0)

            # Write to the file now.
            cv2.rectangle(segment_pixels, rect_start, rect_end, fill_color, -1)
            cv2.rectangle(segment_pixels, rect_start, rect_end, border_color, 2)

        # Return the set here.
        return start_points, segment_pixels