import os                   # File operations and helpful sys actions.
import glob                 # File searching for directory.
import asyncio              # Async/parallel operations
from PIL import Image       # Used for pixel minipulation.
from matplotlib import pyplot as plt    # Plotting

class ImagPixelOperations:
    '''
        This class is used to convert all white pixels in an image into transparent ones. 
        We combine this class with image inputs to remove all white pixel space so when we go to process
        images, the actual image extraction is super fast.
    '''

    # Class Variables
    IMAGE_SET = []              # Set of all images to process/pull the white space out of.
    IMAGES_PATH = None          # Formatted path for all images to find. This includes wildcards.
    RECURSIVE_SEARCH = False    # Search all subimages. 
    IMAGE_EXTENSION = None      # Extension of files to find.
    IMAGE_NAME_FILTER = "*"     # Find images only with the given name/wildcard.

    # Constructor for all the stuff needed to modify/import our images.
    # This is used to locate new files, check out the images and store their paths.
    def __init__(self, images_path = "", file_wildcard = "*", use_subdirs = False):
        # If we did not give an images path then return.
        if (images_path == ""): return

        # Store instance values for class variables.
        self.RECURSIVE_SEARCH = use_subdirs     # Store recursive use. 
        self.IMAGE_NAME_FILTER = file_wildcard  # Wildcard for file searching. 

        # If wildcard is not empty, then we append the wildcard to the path of the files. 
        # Then get all the images in that directory. 
        if (self.IMAGE_NAME_FILTER != ""): 
            if (self.RECURSIVE_SEARCH is True): images_path = os.path.join(images_path, file_wildcard)
            else: images_path = images_path + "**\\" + file_wildcard

        # Store the image set here and save the path.
        self.IMAGES_PATH = images_path
        self.IMAGE_SET = glob.iglob(images_path, self.RECURSIVE_SEARCH)


    # BG Wrapper for helpers to create async loops for parallel processing for the color 
    # extraction/removal setup.
    def __background(f):
        def __wrapped(*args, **kwargs):
            return asyncio.get_event_loop().run_in_executor(None, f, *args, **kwargs)

        return __wrapped


    # Remove whitepsace from ALL images in the current image set. 
    # This requires you to want the WHOLE SET OF IMAGES to remove a given color. 
    # Pass in the color to remove, and an optional wildcard for image names.
    def remove_from_many(self, bg_color_values, image_filter = "*", debug_output = False):
        # This holds the split list names into chunks and finds equal size parts of said list. 
        # Each chunk is put into the Split list set.
        split_list_set = []    
        filtered_image_names = [img for img in self.IMAGE_SET if image_filter in img]
        for i in range(0, len(filtered_image_names), 10):
            split_list_set.append(filtered_image_names[i:i + 10])

        # Inform we're starting to do some kinda looping.
        if (debug_output): 
            print (f"SETTING UP LOOPS FOR IMAGE CLEANING. ({len(split_list_set)} image lists with {len(self.IMAGE_SET)} images total.)")

        # Make a thread pool here.
        # Setup a thread per item in the split list set. 
        for list_index in range(0,len(split_list_set)):
            for image_index in range(0, len(split_list_set[list_index])):
                if (debug_output is True): print (f"LIST:IMAGE - [{list_index}][{image_index}]")
                image_to_clean = split_list_set[list_index][image_index]
                self.remove_color_pixels(image_to_clean, bg_color_values)

        # Print out done ok here.
        if (debug_output): print ("DONE REMOVING COLOR FROM IMAGE SET!")


    # Removes all pixels of the desired color from the imagve provided.
    # Takes in the path of an image and the color to remove. Default is white. 
    # Isolate Color mode will turn any pixels that do not match the given value to 0 alpha.
    # So this would isolate all of the color given instead of remove all of the color given.
    @ __background
    def isolate_pixels_by_color(image_path, bg_color_values = [255, 255, 255, 255], isolate_color = False):
        # Load the image file and convert it to RGBA
        imgage_content = Image.open(image_path)
        imgage_content = imgage_content.convert("RGBA")

        # Store the pixel data and use a recursive for loop to find all the white pixel spots. 
        image_pixels = imgage_content.load()

        # Make easy to use int values for looping here.
        rVal = bg_color_values[0]
        gVal = bg_color_values[1]
        bVal = bg_color_values[2]
        alphaVal = bg_color_values[3]

        # Loop all the pixels here and replace the background color with nothing. 
        width, height = imgage_content.size
        for y in range(height):
            for x in range(width):
                if (isolate_color is not True):
                    if image_pixels[x, y] == (rVal, gVal, bVal, alphaVal):
                        image_pixels[x, y] = (rVal, gVal, bVal, 0)
                else:
                    if image_pixels[x, y] != (rVal, gVal, bVal, alphaVal):
                        image_pixels[x, y] = (rVal, gVal, bVal, 0)


        # Save the output file here. 
        dir_head, file_name = os.path.split(image_path)
        image_name_only = os.path.splitext(file_name)
        new_img_path = os.getcwd() + "\\RuntimeData\\" + image_name_only[0] + "_BG_REMOVED" + image_name_only[1]
        imgage_content.save(new_img_path)

        # Give back the path of our new image.
        return new_img_path




    